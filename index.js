const Route = require('route-parser');

/**
 * The Router class is exported and then can be used in the application
 *
 * @class Router
 */
class Router {
    /**
     * Creates an instance of Router.
     * @param {Array} [allowedMethods=[]] Methods are more like categories and they serve to organize you code.
     *                                    Just pass an array of strings and the appropriate methods will be created on the router instance
     *                                    For example passing ['get'] would create a method router.get(...)
     *                                    A default method router.on is always created
     * @memberof Router
     */
    constructor(allowedMethods = []) {
        this.socket = null;

        this.allowedMethods = ['on', ...allowedMethods];

        this.routes = new Map()

        this.allowedMethods.forEach((method) => {
            this.routes.set(method, []);

            this[method] = (route, cb) => {
                this.route(method, route, cb)
            }
        });
    }

    /**
     * Attach a router to socket instance.
     *
     * @param {Instance} socket Instance of the socket class passed from socket.use
     * @param {Object} packet The message passed from the event
     * @param {Function} next Function called at the end of the middleware to pass the event to next middleware
     * @returns null / Error / next function
     * @memberof Router
     */
    attach(socket, packet, next) {
        this.socket = socket;

        const route = packet[0];
        const request = packet[1];

        let params = false;
        let matchedRoute = null;

        if (!request.method) request.method = 'on';

        if (!this.routes.get(request.method)) {
            return next(new Error(`404 Route ${request.method.toUpperCase()} ${route} was not found.`));
        }

        this.routes.get(request.method).forEach((routeDef) => {
            const match = routeDef.route.match(route);
            if (match) {
                params = match;
                matchedRoute = routeDef
            }
        });

        if (!params) {
            return next(new Error(`404 Route ${request.method.toUpperCase()} ${route} was not found.`));
        }

        const ctx = {
            request: {
                route,
                params,
                ...request
            },
            response: {}
        }

        matchedRoute.callback(socket, ctx, next);
    }

    /**
     * A general method to create a route. This method is used by all the other shorthand methods based on allowedMethods in the constructor.
     * You can use this method as standalone as well
     *
     * @param {String} method name of the method you want to create
     * @param {String} route definition of the route. Has to be compatible with route-parser library (https://www.npmjs.com/package/route-parser)
     * @param {Function} cb a callback function
     * @memberof Router
     */
    route(method, route, cb) {
        if (this.routes.get(method)) {
            return this.routes.get(method).push({ route: new Route(route), callback: cb });
        }

        throw new Error(`Method ${method} is not allowed.`);
    }
}

module.exports = Router;