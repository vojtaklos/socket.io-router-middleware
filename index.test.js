const Router = require('./index.js');
const routeParser = require('route-parser');

test('instantiates router with parameters', () => {
    const router = new Router(['test']);

    expect(router.allowedMethods).toEqual(['on', 'test']);
    expect(router.routes.get('on')).toBeDefined()
    expect(router.routes.get('test')).toBeDefined()
    expect(router.test).toBeDefined()
    expect(router.on).toBeDefined()
})

test('adds routes to the instance', () => {
    const router = new Router(['testRoute']);

    router.testRoute('/test', (socket, ctx, next) => {
        return next();
    })

    const testRoute = router.routes.get('testRoute');

    expect(testRoute[0].route).toBeInstanceOf(routeParser);
    expect(testRoute[0].callback).toBeDefined();
})

test('resolves route', (done) => {
    const router = new Router();

    const modelResp = {
        request: {
            route: '/test/testValue',
                params: { testParam: 'testValue' },
            method: 'on',
            payload: {
                hello: 'world'
            }
        },
        response: {}
    }

    router.on('/test/:testParam', (socket, ctx, next) => {
        try {
            expect(ctx).toEqual(modelResp);
            done();
        } catch (error) {
            done(error);
        }
    })

    // call a route programmatically
    router.attach(null, ['/test/testValue', {
        payload: {hello: 'world'}
    }]);
})

test('throws on undefined route', (done) => {
    const router = new Router();

    function next(error) {
        expect(error).toBeInstanceOf(Error);
        done();
    }

    router.on('/test/:testParam', (socket, ctx, next) => {
        done(new Error())
    })

    // call a route programmatically
    router.attach(null, ['/wrong/route', {}], next);
})

test('throws on wrong method', (done) => {
    const router = new Router();

    function next(error) {
        expect(error).toBeInstanceOf(Error);
        done();
    }

    router.on('/test/:testParam', (socket, ctx, next) => {
        done(new Error())
    })

    // call a route programmatically
    router.attach(null, ['/wrong/testValue', {
        method: 'wrong'
    }], next);
})

test('throws on method not allowed', (done) => {
    const router = new Router();

    expect(router.wrong).toBeUndefined()

    try {
        router.route('wrong')
        done(new Error())
    } catch (error) {
        expect(error).toBeInstanceOf(Error);
        done()
    }
})