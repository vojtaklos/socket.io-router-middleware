const app = require('express')()
const http = require('http').Server(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 3000;


app.get('/', (req, res) => {
    res.sendFile(__dirname + '/example.html');
});

// Require the router
const IORouter = new require('socket.io-router-middleware');

// Instantiate the router
const iorouter = new IORouter(['get']);

// Add router paths
iorouter.on('/api/:param?sort=:sort', (socket, ctx, next) => {
    ctx.response = { hello: 'from server' }

    socket.emit('response', ctx);

    // Don't forget to call next() at the end to enable passing to other middlewares
    next();
});

// On client connection attach the router
io.on('connection', function (socket) {
    socket.use((packet, next) => {
        // Call router.attach() with the client socket as the first parameter
        iorouter.attach(socket, packet, next)
    });
});

http.listen(port, function () {
    console.log('listening on *:' + port);
});